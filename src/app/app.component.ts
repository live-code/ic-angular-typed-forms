import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <button routerLink="demo1">Demo1</button>
    <button routerLink="demo2">Demo2</button>
    <button routerLink="demo3">Demo3</button>
    <button routerLink="demo4">Demo4</button>
    <button routerLink="demo5">Demo5</button>
    <button routerLink="demo6">Demo6</button>
    <button routerLink="demo7">Demo7</button>
    <hr>
    <div class="container">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class AppComponent {
  title = 'ic-forms';
}
