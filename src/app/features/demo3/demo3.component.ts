import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, SharedModule],
  template: `
    <form [formGroup]="form">
  
      <app-my-input
        label="Your Name"
        formControlName="name"
        required
        minlength="3"
        alphaNumeric
      ></app-my-input>
      
     
      <app-my-input
        label="Your Surname"
        formControlName="surname"
      ></app-my-input>
      
      <app-color-picker 
        formControlName="color"></app-color-picker>

      <app-rating 
        formControlName="rate"></app-rating>
    </form>
    
    <pre>{{form.valid | json}}</pre>
  `,
})
export class Demo3Component {
  form = this.fb.group({
    name: ['fabio', [Validators.required, Validators.minLength(3)]],
    surname: '',
    color: ['', Validators.required],
    rate: 3
  })

  constructor(private fb: FormBuilder) {


  }
}

