import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-demo5',
  template: `
   
    <form [formGroup]="form">
      <input type="text" placeholder="order Id" formControlName="orderId" class="form-control">
    
      {{form.get('anagrafica')?.errors | json}}
      <app-anagrafica formControlName="anagrafica"></app-anagrafica>
      
      <div formGroupName="carInfo">
        <h1>Car Info</h1>
        <input type="text" placeholder="Brand" formControlName="brand">
        <input type="text" placeholder="Name" formControlName="name">
      </div>
      
      <button [disabled]="form.invalid">SAVE</button>
    </form>
    
    <pre>valid: {{form.valid | json}}</pre>
    <pre>value: {{form.value | json}}</pre>
  `,
})
export class Demo5Component {
  form = this.fb.group({
    orderId: ['', Validators.required],
    anagrafica: { name: '', surname: ''},
    carInfo: this.fb.group({
      brand: '',
      name: ''
    })
  })

  constructor(private fb: FormBuilder) {

    console.log(this.form.get('anagrafica.name')?.value)

    const res = {
      orderId: 'A123',
      anagrafica: {
        name: 'x',
        surname: 'y'
      }
    }

    /*setTimeout(() => {
      this.form.patchValue(res)
    }, 2000)*/


/*
    // this.form.patchValue(res);
    this.form.reset({
      orderId: '999'
    })*/
  }
}
