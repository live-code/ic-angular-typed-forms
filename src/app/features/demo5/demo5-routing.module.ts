import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo5Component } from './demo5.component';

const routes: Routes = [{ path: '', component: Demo5Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Demo5RoutingModule { }
