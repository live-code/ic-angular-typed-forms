import { Component } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';


type Role = 'admin' | 'guest' | '';

interface MyForm {
  name: FormControl<string>;
  role: Role;
  subscription?: FormControl<boolean>;
}

@Component({
  selector: 'app-demo4',
  template: `
    <form [formGroup]="form">
      <input type="text" formControlName="name" class="form-control">
      
      <select formControlName="role" class="form-control">
        <option value="">Select a role</option>
        <option value="admin">Admin</option>
        <option value="guest">Guest</option>
      </select>
      
      <input
        *ngIf="form.get('role')?.value === 'guest'"
        type="checkbox" formControlName="subscription">
    </form>
    
    <button (click)="addField()"></button>
    
    <pre>{{form.value | json}}</pre>
  `,
})
export class Demo4Component {
  form = this.fb.group<MyForm>({
    name: new FormControl('', { nonNullable: true }),
    role: '',
  })

  constructor(private fb: FormBuilder) {
    this.form.patchValue({
      name: 'fab',
      role: ''
    })

    this.form.get('role')?.valueChanges
      .subscribe(role => {
        if (role === 'guest') {
          this.form.addControl('subscription', new FormControl(false, {nonNullable: true }))
        } else {
          this.form.removeControl('subscription')
        }
      })

    /*setTimeout(() => {
      this.form.addControl('subscription', new FormControl(false, {nonNullable: true }))
    }, 2000)*/
  }

  addField() {

  }
}
