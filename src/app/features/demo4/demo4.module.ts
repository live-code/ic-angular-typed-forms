import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { Demo4RoutingModule } from './demo4-routing.module';
import { Demo4Component } from './demo4.component';


@NgModule({
  declarations: [
    Demo4Component
  ],
  imports: [
    CommonModule,
    Demo4RoutingModule,
    ReactiveFormsModule
  ]
})
export class Demo4Module { }
