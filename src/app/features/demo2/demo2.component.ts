import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { filter } from 'rxjs';

@Component({
  selector: 'app-demo2',
  template: `
    
    <form [formGroup]="form" (submit)="save()">
      
      <div>
        <input type="checkbox" id="chk" formControlName="isCompany"> 
        <label for="chk">are you a company?</label>
      </div>
      
      <input 
        class="form-control"
        type="text" placeholder="company name" formControlName="name"
        [ngClass]="{
          'is-invalid': form.get('name')?.invalid,
          'is-valid': form.get('name')?.valid
        }"
      >
      <app-error-minlength-bar [errors]="form.get('name')?.errors"></app-error-minlength-bar>
      <div *ngIf="form.get('name')?.errors?.['required']">required</div>
      <div *ngIf="form.get('name')?.errors?.['minlength']">min 3 chars</div>
      
      <!--<app-input-errors-msg [errors]="form.get('name')?.errors"> </app-input-errors-msg>-->
      

      <br>
      <input
        class="form-control"
        [ngClass]="{
          'is-invalid': form.get('vat')?.invalid,
          'is-valid': form.get('vat')?.valid
        }"
        type="text" placeholder="your vat" 
        formControlName="vat"
      >

      <div *ngIf="form.get('vat')?.errors?.['required']">required</div>
      <div *ngIf="form.get('vat')?.errors?.['alphaNumeric']">no symbols</div>

      <div class="btn-group mt-2">
        <button type="submit" class="btn btn-primary" [disabled]="form.invalid">save</button>
        <button type="button" class="btn btn-outline-primary" (click)="clear()">Clear</button>
      </div>
    </form>
    
    <pre>valid: {{form.valid}}</pre>
    <pre>errors: {{form.get('vat')?.errors | json}}</pre>
    
    
    <button (click)="populate()">populate</button>
  `,
  styles: [
  ]
})
export class Demo2Component {

  form = this.fb.nonNullable.group(
    {
      isCompany: true,
      name: ['', [Validators.required, Validators.minLength(3)]],
      vat: ['', [Validators.required, alphaNumericValidator]]
    },
  )

  constructor(private fb: FormBuilder) {
    this.form.get('isCompany')?.valueChanges
      .subscribe(isCompany => {
        if (isCompany) {
          this.form.get('vat')?.enable()
        } else {
          this.form.get('vat')?.disable()
        }
      })
  }

  save() {
    console.log('HTTP', this.form.value)
    console.log('HTTP', this.form.getRawValue())
  }

  clear() {
    this.form.reset()
  }

  populate() {
 /*   this.form.setValue({ name: 'abc', vat: 123 })*/
    this.form.patchValue({ vat: '123' })
  }
}



// share/utils/alpha-numeric.validator.ts
export const ALPHA_NUMERIC_REGEX = /^\w+$/

export function alphaNumericValidator(control: FormControl): ValidationErrors | null {
  if (control.value && !control.value.match(ALPHA_NUMERIC_REGEX)) {
    return { alphaNumeric: true };
  }

  return null;
}
