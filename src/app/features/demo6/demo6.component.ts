import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { AbstractControl, FormBuilder, ValidatorFn, Validators } from '@angular/forms';
import { interval, timer } from 'rxjs';
import { UserValidatorService } from '../../shared/forms/user-validator.service';

@Component({
  selector: 'app-demo6',
  template: `
    <form [formGroup]="form">
      <input type="text" placeholder="name" formControlName="name">
      <pre>{{form.get('name')?.pending | json}}</pre>
      <hr>
      
      <div formGroupName="passwords">
        <input type="text" placeholder="Password" formControlName="pass1">
        <input
          class="form-control"
          [ngClass]="{'is-invalid': form.get('passwords.pass2')?.invalid}"
          type="text" placeholder="Confirm Password" formControlName="pass2"
        >
        <pre>{{form.get('passwords.pass2')?.errors | json}}</pre>
      </div>
      
      <button [disabled]="form.invalid || form.pending">SAVE</button>
    </form>
    
    <pre>{{form.get('passwords')?.errors | json}}</pre>
    <pre>{{form.valid}}</pre>
  `,
  styles: [
  ]
})
export class Demo6Component {
  form = this.fb.group(
    {
      name: [
        '',
        [Validators.required, Validators.minLength(3)],
        this.userValidator.checkUsername()
      ],

      passwords: this.fb.group(
        {
          pass1: ['', [Validators.required, Validators.minLength(3)]],
          pass2: ['', [Validators.required, Validators.minLength(3)]],
        },
        {
          validators: passwordsMatch()
        }
      )
    },
  )
  constructor(
    private fb: FormBuilder,
    private userValidator: UserValidatorService
  ) {

  }
}


export function passwordsMatch(): ValidatorFn {

  return (group: AbstractControl) => {
    const p1 = group.get('pass1');
    const p2 = group.get('pass2');
    const isMatch = p1?.value === p2?.value;

    /*
    if (isMatch) {
      group.get('pass2')?.setErrors({ ...group.get('pass2')?.errors })
    } else {
      group.get('pass2')?.setErrors({ notMatch: true })
    }*/


    return isMatch
      ? null
      : { passwordNotMatch: true }
  };
}
