import { Component } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-demo7',
  template: `
    <form [formGroup]="form">
      <input type="text" formControlName="name">
      
      <div formArrayName="items">
        <div 
          class="d-flex"
          *ngFor="let item of items.controls; let i = index; let last = last"
          [formGroupName]="i"
        >
          <i class="fa fa-check" *ngIf="item.valid"></i>
          <input class="w" type="text" placeholder="item name" formControlName="name">
          <input class="w" type="text" placeholder="item cost" formControlName="cost">
          <i class="fa fa-trash" (click)="removeItem(i)" *ngIf="items.controls.length > 1"></i>
          <i class="fa fa-plus" (click)="addItem()" *ngIf="item.valid && last"></i>
        </div>
      </div>
      
    </form>
    
    <pre>{{form.value | json}}</pre>
  `,
  styles: [`
    .w { width: 100px}
  `]
})
export class Demo7Component {
  form = this.fb.group({
    name: '',
    items: this.fb.array<any>([])
  })
  items = this.form.get('items') as FormArray;

  constructor(private fb: FormBuilder) {
    this.addItem();
  }

  addItem() {
    this.items.push(
      this.fb.group({
        name: ['', Validators.required],
        cost: ['', Validators.required]
      })
    )
  }

  removeItem(i: number) {
    this.items.removeAt(i)
  }
}
