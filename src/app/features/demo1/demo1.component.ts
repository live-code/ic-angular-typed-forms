import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, ValidationErrors, Validators } from '@angular/forms';
import {
  BehaviorSubject,
  combineLatest,
  debounceTime,
  distinctUntilChanged,
  filter,
  fromEvent,
  interval,
  map, mergeAll, mergeMap
} from 'rxjs';

const REGEX_ALPHA_NUMERIC = /^\w+$/


export type Role = 'admin' | 'mod';

@Component({
  selector: 'app-demo1',
  template: `
    <h1>Reactive Forms</h1>
    
    <div style="width: 200px">
      <input 
        style="width: 100%"
        [ngClass]="{ hasError:input.invalid }"
        type="text" [formControl]="input"
      >
      <app-error-minlength-bar [errors]="input.errors"></app-error-minlength-bar>
      <app-input-errors-msg [errors]="input.errors"></app-input-errors-msg>
      
     <div *ngIf="input.errors?.['pattern']">Alphanumeric only</div> 
    </div>
    
    <div>
      <select [formControl]="select">
        <option value="">Select a role</option>
        <option value="admin">Admin</option>
        <option value="mod">Moderator</option>
      </select>
    </div>
    
    <div>
      <button (click)="populate()">populate</button>
      <button (click)="clean()">Clean</button>
    </div>
    
    <pre>value: {{select.value}}</pre>
    <pre>errors: {{select.errors | json}}</pre>
  `,
  styles: [`
    .hasError { background-color: red }
  `]
})
export class Demo1Component  {
  input = new FormControl('', {
    nonNullable: true,
    validators: [
      Validators.required, Validators.minLength(4), Validators.pattern(REGEX_ALPHA_NUMERIC)
    ]
  });

  select = new FormControl<Role>('admin');

  clean() {
    this.input.reset()
    //console.log(this.input.value)
  }

  populate() {
    // this.input.setValue('ciao')
    this.select.setValue('mod')
  }


}
