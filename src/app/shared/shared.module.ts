import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ErrorMinlengthBarComponent } from './forms/error-minlength-bar.component';
import { InputErrorsMsgComponent } from './forms/input-errors-msg.component';
import { GetPercPipe } from './forms/get-perc.pipe';
import { ColorPickerComponent } from './forms/color-picker.component';
import { RatingComponent } from './forms/rating.component';
import { MyInputComponent } from './forms/my-input.component';
import { AnagraficaComponent } from './forms/anagrafica.component';



@NgModule({
  declarations: [
    ErrorMinlengthBarComponent,
    InputErrorsMsgComponent,
    GetPercPipe,
    ColorPickerComponent,
    RatingComponent,
    MyInputComponent,
    AnagraficaComponent
  ],
  exports: [
    ErrorMinlengthBarComponent,
    InputErrorsMsgComponent,
    GetPercPipe,
    ColorPickerComponent,
    RatingComponent,
    MyInputComponent,
    AnagraficaComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ]
})
export class SharedModule { }
