import { Component, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-input-errors-msg',
  template: `
    <div *ngIf="errors as err">
      <div *ngIf="err['required']">required</div>
      <div *ngIf="err['minlength']">
        missing {{err['minlength']['requiredLength'] - err['minlength']['actualLength']}} chars
      </div>
    </div>
  `,
})
export class InputErrorsMsgComponent {
  @Input() errors: ValidationErrors | null | undefined = null;
}
