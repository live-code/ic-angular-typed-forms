import { Component, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-error-minlength-bar',
  template: `
    <div 
      class="bar" 
      [style.width]="errors! | getPerc"
    ></div>
  `,
  styles: [`
    .bar {
      width: 100%;
      background-color: gray;
      height: 4px;
      border-radius: 10px;
      margin-top: 5px;
      transition: width 0.5s ease-in-out;
    } 
  `]
})
export class ErrorMinlengthBarComponent {
  @Input() errors: ValidationErrors | null | undefined = null;
}
