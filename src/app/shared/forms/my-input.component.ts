import { Component, Injector, Input } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';

@Component({
  selector: 'app-my-input',
  template: `
    <div>
      {{ngControl.errors | json}}
      <label>{{label}}</label>
      <br>
      <input type="text" 
             [formControl]="input"
             (blur)="onTouched()"
      >
    </div>
  `,
  providers:[
    { provide: NG_VALUE_ACCESSOR, useExisting: MyInputComponent, multi: true }
  ],
})
export class MyInputComponent implements ControlValueAccessor {
  @Input() label = ''
  onTouched!: () => void;
  input = new FormControl();
  ngControl!: NgControl;

  constructor( private inj: Injector) {}

  ngOnInit() {
    this.ngControl = this.inj.get(NgControl)
  }

  registerOnChange(fn: any): void {
    this.input.valueChanges.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(text: string): void {
    this.input.setValue(text)
  }
}


