import { Component } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormBuilder,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, ValidationErrors,
  Validator,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-anagrafica',
  template: `
    <div [formGroup]="form">
      <h1>
        <i class="fa fa-check" *ngIf="form.get('anagrafica')?.valid"></i>
        Anagrafica
      </h1>
      <div *ngIf="form.invalid">not valid</div>
      
      <input
        type="text" placeholder="Name" formControlName="name"
        class="form-control"
        [ngClass]="{
            'is-invalid': form.get('anagrafica.name')?.invalid
          }"
      >
      <input type="text" placeholder="Surname" formControlName="surname" class="form-control">
    </div>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, multi:true, useExisting: AnagraficaComponent},
    { provide: NG_VALIDATORS, multi:true, useExisting: AnagraficaComponent},
  ]
})
export class AnagraficaComponent implements ControlValueAccessor, Validator{
  form = this.fb.group({
    name: ['', Validators.required],
    surname: ['', Validators.required]
  })

  constructor(private fb: FormBuilder) {
  }

  registerOnChange(fn: any): void {
    this.form.valueChanges.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
    this.form.setValue(obj)
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return this.form.invalid ?
      { invalidFields: true } :
      null;
  }

}
