import { Pipe, PipeTransform } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Pipe({
  name: 'getPerc'
})
export class GetPercPipe implements PipeTransform {

  transform(errors: ValidationErrors | null): unknown {
    if (errors) {
      const minLengthError = errors['minlength']
      if (minLengthError) {
        return ((minLengthError.actualLength / minLengthError.requiredLength) * 100) + '%'
      }
      return 0;
    } else {
      return 0;
    }
  }

}
