import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { debounce, debounceTime, map, mergeMap, timer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserValidatorService {

  constructor(
    private http: HttpClient
  ) { }

  checkUsername(): AsyncValidatorFn {
    return (control: AbstractControl) => {
      return timer(1000)
        .pipe(
          mergeMap(() => this.http.get<any[]>(`https://jsonplaceholder.typicode.com/users?username=${control.value}`)
            .pipe(
              map(res => res.length > 0 ? { usernameExists: true } : null)
            ))
        )
    }
  }
}


