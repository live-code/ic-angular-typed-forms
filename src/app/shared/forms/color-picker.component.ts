import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-color-picker',
  template: `
    <div class="d-flex">
      <div
        *ngFor="let c of colors"
        class="cell"
        (click)="notifyChange(c)"
        [ngClass]="{active: c === selectedColor}"
        [style.background-color]="c"
      >
      </div>
    </div>
    {{selectedColor}}
  `,
  styles: [`
    .cell {
      width: 30px;
      height: 30px;
      border: 1px solid gray
    }
    .active {
      border: 5px solid black
    }
  `],
  providers: [
    { provide: NG_VALUE_ACCESSOR, multi: true, useExisting: ColorPickerComponent}
  ]
})
export class ColorPickerComponent implements ControlValueAccessor{
  @Input() colors = [
    '#F44336', '#90CAF9', '#FFCDD2', '#69F0AE', '#AED581', '#FFE082'
  ];
  selectedColor: string | undefined;
  onChange!: (newColor: string) => void;
  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(color: string): void {
    this.selectedColor = color;
  }

  notifyChange(newColor: string) {
    this.selectedColor = newColor
    this.onChange(newColor)
    this.onTouched()
  }
}
